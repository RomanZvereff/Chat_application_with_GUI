package com.levelup.network;

public interface TCPConnectionListener {    //универсальный интерфейс с помощью которого можно реализовать поведение
                                            //как сервера так и клиента при обработке данных
    void onConnectionReady (TCPConnection tcpConnection);               //запуск соединения
    void onReceiveString (TCPConnection tcpConnection, String value);   //принятие строки
    void onDisconnect (TCPConnection tcpConnection);                    //разрыв соединения
    void onException(TCPConnection tcpConnection, Exception e);         //при возникновении исключения
}