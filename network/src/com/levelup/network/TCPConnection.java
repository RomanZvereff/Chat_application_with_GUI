package com.levelup.network;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;

public class TCPConnection {                                    //КЛАСС СОЕДИНЕНИЯ

    private final Socket socket;                                //сокет соединения
    private final Thread thread;                                //поток для входящих сообщений
    private final TCPConnectionListener eventListener;          //"слушатель" событий
    private final BufferedReader in;                            //поток ввода, который считывает входящие данные
    private final BufferedWriter out;                           //поток вывода,

    //конструктор создаёт Сокет по параметрам IP-адреса и Порта
    public TCPConnection (TCPConnectionListener eventListener, String ipAddress, int port) throws IOException {
        this(eventListener, new Socket(ipAddress, port));
    }

    //конструктор принимающий на вход объект Сокета и создает с ним соединение
    public TCPConnection (TCPConnectionListener eventListener, Socket socket) throws IOException {
        this.eventListener = eventListener;
        this.socket = socket;
        //получение потока ввода с указанием кодировки
        in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8")));
        //получение потока вывода с указанием кодировки
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")));
        thread = new Thread(new Runnable() {       //реализуем интерфейс Runnable для считывания входящего соединения
            @Override
            public void run() {
                try {
                    eventListener.onConnectionReady(TCPConnection.this);    //передаём интерфейсу экземпляр класс TCPConnection
                    while (!thread.isInterrupted()) {                                    //пока поток не прерван получаем строку и отдаем eventListener
                        eventListener.onReceiveString(TCPConnection.this, in.readLine());
                    }
                } catch (IOException e) {
                    eventListener.onException(TCPConnection.this, e);
                } finally {
                    eventListener.onDisconnect(TCPConnection.this);
                }
            }
        });
        thread.start();
    }
        //методы синхронизированы для потокобезопасности
    public synchronized void sendString(String value) {   //метод отправки сообщений
        try {
            out.write(value + "\r\n");
            out.flush();            //сбрасывает буфер и отправляет строку сообщения
        } catch (IOException e) {
            eventListener.onException(TCPConnection.this, e);
            disconnect();
        }
    }

    public synchronized void disconnect() {     //метод для разрыва соединения в любой момент
        thread.interrupt();
        try {
            socket.close();
        } catch (IOException e) {
            eventListener.onException(TCPConnection.this, e);
        }
    }

    @Override               //для информации
    public String toString(){
        return "TCPConnection: " + socket.getInetAddress() + ": " + socket.getPort();
    }
}